import sys
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import QMainWindow, QWidget, QLabel, QLineEdit, QMessageBox, QPlainTextEdit, QTableWidget, \
    QTableWidgetItem
from PyQt5.QtWidgets import QPushButton
from PyQt5.QtCore import QSize
from db import db


class CreateWindow(QMainWindow):
    def __init__(self, parent=None):
        super(CreateWindow, self).__init__(parent)
        self.setMinimumSize(QSize(950, 600))
        self.parent = parent

        self.nameLabel = QLabel(self)
        self.nameLabel.setText('Название:')
        self.nameLine = QLineEdit(self)

        self.nameLine.resize(200, 32)
        self.nameLine.move(120, 20)
        self.nameLabel.move(20, 20)

        self.priceLabel = QLabel(self)
        self.priceLabel.setText('Цена:')
        self.priceLine = QLineEdit(self)

        self.priceLine.resize(200, 32)
        self.priceLine.move(120, 60)
        self.priceLabel.move(20, 60)

        self.linkLabel = QLabel(self)
        self.linkLabel.setText('Ссылка:')
        self.linkLine = QLineEdit(self)

        self.linkLine.resize(200, 32)
        self.linkLine.move(120, 100)
        self.linkLabel.move(20, 100)

        # Add text field
        self.noteLabel = QLabel(self)
        self.noteLabel.setText('Примечание:')
        self.noteTextEdit = QPlainTextEdit(self)
        self.noteTextEdit.resize(200, 100)
        self.noteTextEdit.move(120, 140)
        self.noteLabel.move(20, 140)

        self.button = QPushButton("Сохранить", self)
        self.button.clicked.connect(self.save)
        self.button.move(20, 250)
        self.button.show()

    def save(self):
        cursor = db.cursor()
        sql = "INSERT INTO wishlist (name, price, link, note) VALUES (%s, %s, %s, %s)"
        name = self.nameLine.text()
        price = self.priceLine.text()
        link = self.linkLine.text()
        note = self.noteTextEdit.toPlainText()
        values = (name, price, link, note)
        cursor.execute(sql, values)
        db.commit()
        QMessageBox.about(self, 'Успешно', 'Запись успешно добавлена')
        self.close()


class UpdateWindow(QMainWindow):
    def __init__(self, parent=None, record_id=None, record_name=None, record_price=None, record_link=None, record_note=None):
        super(UpdateWindow, self).__init__(parent)
        self.setMinimumSize(QSize(950, 600))
        self.parent = parent
        self.record_id = record_id

        self.nameLabel = QLabel(self)
        self.nameLabel.setText('Название:')
        self.nameLine = QLineEdit(self)
        self.nameLine.setText(record_name)

        self.nameLine.resize(200, 32)
        self.nameLine.move(120, 20)
        self.nameLabel.move(20, 20)

        self.priceLabel = QLabel(self)
        self.priceLabel.setText('Цена:')
        self.priceLine = QLineEdit(self)
        self.priceLine.setText(record_price)

        self.priceLine.resize(200, 32)
        self.priceLine.move(120, 60)
        self.priceLabel.move(20, 60)

        self.linkLabel = QLabel(self)
        self.linkLabel.setText('Ссылка:')
        self.linkLine = QLineEdit(self)
        self.linkLine.setText(record_link)

        self.linkLine.resize(200, 32)
        self.linkLine.move(120, 100)
        self.linkLabel.move(20, 100)

        # Add text field
        self.noteLabel = QLabel(self)
        self.noteLabel.setText('Примечание:')
        self.noteTextEdit = QPlainTextEdit(self)
        self.noteTextEdit.resize(200, 100)
        self.noteTextEdit.move(120, 140)
        self.noteLabel.move(20, 140)
        self.noteTextEdit.setPlainText(record_note)

        self.button = QPushButton("Обновить", self)
        self.button.clicked.connect(self.update)
        self.button.move(20, 250)
        self.button.show()

    def update(self):
        cursor = db.cursor()
        sql = "UPDATE wishlist SET name = %s, price = %s, link = %s, note = %s WHERE id = " + str(self.record_id)
        name = self.nameLine.text()
        price = self.priceLine.text()
        link = self.linkLine.text()
        note = self.noteTextEdit.toPlainText()
        values = (name, price, link, note)
        cursor.execute(sql, values)
        db.commit()
        QMessageBox.about(self, 'Успешно', 'Запись успешно Обновлена')
        self.parent.load_data()
        self.close()


class MainWindow(QMainWindow):

    createWindow = None
    updateWindow = None

    def __init__(self):
        QMainWindow.__init__(self)

        self.setMinimumSize(QSize(950, 600))
        self.setWindowTitle("Wishlist")

        self.table = QTableWidget(self)
        self.table.setColumnCount(7)
        # Устанавливаем заголовки таблицы
        self.table.setHorizontalHeaderLabels(["id", "Название", "Цена", "Ссылка", "Примечание", "", ""])
        self.table.move(20, 20)
        self.table.resize(930, 250)
        # self.table.resizeColumnsToContents()
        self.load_data()

        self.createButton = QPushButton("Создать запись", self)
        self.createButton.clicked.connect(self.create_record)
        self.createButton.move(20, 300)
        self.createButton.show()
        self.createWindow = CreateWindow(self)

    def create_record(self):
        self.createWindow.show()

    def load_data(self):
        self.table.setRowCount(0)
        sql = "SELECT * FROM wishlist"
        cursor = db.cursor()
        cursor.execute(sql)
        for row, form in enumerate(cursor):
            self.table.insertRow(row)
            for column, item in enumerate(form):
                print(str(item))
                item = QTableWidgetItem(str(item))
                item.setFlags(QtCore.Qt.ItemIsEnabled)
                self.table.setItem(row, column, item)

            btn = QPushButton(self.table)
            btn.setText('Обновить')
            btn.clicked.connect(self.update_cell)
            self.table.setCellWidget(row, 5, btn)

            btn = QPushButton(self.table)
            btn.setText('Удалить')
            btn.clicked.connect(self.delete_cell)
            self.table.setCellWidget(row, 6, btn)

    def update_cell(self):
        row = self.table.currentRow()
        record_id = self.table.item(row, 0).text()
        name = self.table.item(row, 1).text()
        price = self.table.item(row, 2).text()
        link = self.table.item(row, 3).text()
        note = self.table.item(row, 4).text()
        self.updateWindow = UpdateWindow(self, record_id, name, price, link, note)
        self.updateWindow.show()

    def delete_cell(self):

        msg_box = QMessageBox()
        msg_box.setIcon(QMessageBox.Information)
        msg_box.setText("Вы действительно хотите удалить запись?")
        msg_box.setWindowTitle("Удалить")
        msg_box.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)

        return_value = msg_box.exec()
        if return_value == QMessageBox.Ok:
            row = self.table.currentRow()
            item = self.table.item(row, 0)
            record_id = item.text()
            sql = "DELETE FROM wishlist WHERE id = " + str(record_id)
            cursor = db.cursor()
            cursor.execute(sql)
            db.commit()
            self.load_data()


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    mainWin = MainWindow()
    mainWin.show()
    sys.exit(app.exec_())
