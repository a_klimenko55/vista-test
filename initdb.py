from db import db

cursor = db.cursor()
cursor.execute("SHOW TABLES")

exist = False
for result in cursor:
    if 'wishlist' in result:
        exist = True
        break

if not exist:
    cursor.execute("CREATE TABLE wishlist"
                   " (id INT AUTO_INCREMENT PRIMARY KEY,"
                   " name VARCHAR(255) NOT NULL,"
                   " price DOUBLE NOT NULL,"
                   " link VARCHAR(255) NOT NULL,"
                   " note TEXT)")
